#include "Room.h"
#include "GlobalData.h"

#include <iostream>
using namespace std;

namespace LeesEngine
{
	Room::Room() /*: m_players(25, 5) , m_enemies(25, 5), m_items(25, 5)*/
	{
		m_roomNo = 0;
		m_roomType = "";
		m_dangerLevel = 1;
		//m_exits = {1, 1, 1, 1};
		m_exits.north = -1;
		m_exits.south = -1;
		m_exits.west = -1;
		m_exits.east = -1;
		m_description = "Wot?";
	}

	Room::Room(short p_roomNo, string p_roomType, short p_dangerLevel, 
			    /*Array<string> p_players, Array<Enemy> p_enemies, Array<Item> p_items,*/
				Exits p_exits, string p_description)
				/*: m_players(p_players) , m_enemies(p_enemies), m_items(p_items)*/
	{
		m_roomNo = p_roomNo;
		m_roomType = p_roomType;
		m_dangerLevel = p_dangerLevel;
		m_exits = p_exits;
		m_description = p_description;
	}

	/*Room::Room(short p_roomNo, string p_roomType, short p_dangerLevel,  Exits p_exits, string p_description)
				: m_players(25, 5) , m_enemies(25, 5), m_items(25, 5)
	{
		m_roomNo = p_roomNo;
		m_roomType = p_roomType;
		m_dangerLevel = p_dangerLevel;
		m_exits = p_exits;
		m_description = p_description;
	}*/

	Room::Room(string p_description)
		        /*: m_players(25, 5) , m_enemies(25, 5), m_items(25, 5)*/
	{
		m_roomNo = 0;
		m_roomType = "";
		m_dangerLevel = 1;
		//m_exits = {1, 1, 1, 1};
		m_exits.north = -1;
		m_exits.south = -1;
		m_exits.west = -1;
		m_exits.east = -1;
		m_description = p_description;
	}

	string Room::getDescription()
	{
		return m_description;
	}

	Item Room::getItem(int index)
	{
		return m_items[index];
	}
}