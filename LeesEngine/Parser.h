/**

'Parser' -- Interface component that processes input from the player into events that alter the game state,
and communicates events that alter the game state to the player.

**/

#ifndef PARSER_H
#define PARSER_H

#include <string>

using namespace std;

class Player;
class Enemy;
class Room;

namespace LeesEngine
{
   class Parser
   {
      public:
		   static string executePlayerInput(string command, Player* player);
		   static string executeEnemyCommand(string command, Enemy* enemy);
		   static string executeGameEvent(string event, Room* occursIn);

       private:
		   static string m_verbBase[21];
   }; 
}

#endif