/**

'TimeManager' -- handles the running of the timer in the game,
should also handle time based events like the enemies' attacks, and the spawning of enemies around the map

**/

#ifndef TIME_MANAGER_H
#define TIME_MANAGER_H

#include "Timer.h"
#include "Enemy.h"

#include <vector>

namespace LeesEngine
{
   class TimeManager
   {
       public:
		  TimeManager( bool dummy );
		  /*static void initialise(); //Duhh... omission of this was an obvious oversight =P*/
	      /*static*/ string runGameEvent();
		  /*static*/ void addEnemyForUpdating(Enemy* e);
		  /*static*/ void updateEnemies(); 
		  /*static*/ bool isRunning();
		  /*static*/ void setRunning(bool p_running);

       private:
	      /*static*/ Timer* m_timer;
		  /*static*/ vector<Enemy*> m_enemyManager;
		  /*static*/ bool m_running; //Used for regulating when to kill the thread that runs this updater
   };
}

#endif