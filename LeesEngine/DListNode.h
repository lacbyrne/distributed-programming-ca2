//This is the class for a list node for a Doubly Linked List; like one that would be used for a multiplayer game. 
//Thus, it must be implemented so that it has pointers to both the next node in the List AND THE PREVIOUS NODE; 

#ifndef DLISTNODE_H
#define DLISTNODE_H

template<class Datatype>
class DListNode
{
  //Attributes
  public:
    Datatype mData; //data stored in the node
	DListNode<Datatype>* mNext; //Pointer to node after this node in the List
    DListNode<Datatype>* mPrevious; //Pointer to node before this node in the List [exclusive attribute to the Doubly Linked List]

	//Constructor
    /*DListNode()
	{
      mNext = 0;
	  mPrevious = 0;
	  mData = 0;
	}*/

	//'InsertAfter' function for adding a new node after this node
	//Takes in one parameter- the Player object to set as 'mData' of this node
	void InsertAfter(Datatype d)
	{
      //create the new node, and assign 'p' as the data of the node
	  DListNode<Datatype>* newNode = new DListNode<Datatype>;
	  newNode->mData = d;

	  newNode->mNext = mNext; //'next' pointer of the newnode equals the one that this node had prior to creating the new node
	  newNode->mPrevious = this; //'previous' pointer of the node points to this node

	  if(mNext != 0) //If the node originally after this node still exists
	  {
        mNext->mPrevious = newNode; //The new node becomes the previous node of the original next node
	  }

	  mNext = newNode; //Make the new node the next node of this node
	}

	//'InsertBefore' function- similiar to 'InsertAfter', except adds a new node before this node in the List
	void InsertBefore(Datatype d)
	{
	  DListNode<Datatype>* newNode = new DListNode<Datatype>;
	  newNode->mData = d;

	  newNode->mPrevious = mPrevious; //The previous node of the new node is the node that used to be previous to the current node
	  newNode->mNext = this; //This node is the next node of the new node

	  if(mPrevious != 0) //If this node has a node before it, that node becomes the node before the new node
	  { 
        mPrevious->mNext = newNode;
	  }

	  mPrevious = newNode;
	}
};

#endif