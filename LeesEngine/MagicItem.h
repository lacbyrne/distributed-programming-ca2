/**

'MagicItem' -- models characteristics of items in the game that when used, evoke a particular special effect.
These may include potions (which target the player character in their effects), 
wands and scrolls (which may often target other actors in the room with their effects).

**/

#ifndef MAGIC_ITEM_H
#define MAGIC_ITEM_H

#include "Item.h"

namespace LeesEngine
{
   class MagicItem : public Item
   {
	   //One thing to bear in mind :
	   //The possibly virtual 'Use' function of Item will be virtual no more >:]
	   public:
		   MagicItem(string p_name, string p_description, int p_sellValue, string p_use,
				     string p_effect, short p_charges, short p_range, int p_rechargeTime);
		   string getEffect();
		   short getCharges();
		   short getRange();
		   int getRechargeTime();

       private:
		   string m_effect;
		   short m_charges;
		   short m_range;
		   int m_rechargeTime;
   };
}

#endif