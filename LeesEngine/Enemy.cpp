#include "Enemy.h"

namespace LeesEngine
{
	Enemy::Enemy() : Actor()
	{
		m_xpYield = 0;
		m_aggression = 0;
		m_ally = "";
		m_specName = "";
		m_specPower = 0;
		m_specAcc = 0;
		m_specChance = 0;
		m_specRecharge = 0;
		m_description = "";
	}

	Enemy::Enemy(string p_name, short p_HP, /*short**/ActorAttributes p_attributes, short p_roomNo, short p_x, short p_y,
				  int p_xpYield, short p_aggression, string p_ally, string p_specName, short p_specPower, short p_specAcc, short p_specChance,
				  int p_specRecharge, string p_description)
				  : Actor(p_name, p_HP, p_attributes, p_roomNo, p_x, p_y)
	{

	}

	void Enemy::move(short p_direction)
	{
		
	}

	int Enemy::getAttackTime()
	{
	   return 0;
	}
 
	short Enemy::getAttackAccuracy()
	{
		return 0;
	}

	short Enemy::getAttackPower()
	{
		return 0;
	}

	short Enemy::getEvasion()
	{
		return 0;
	}

	short Enemy::getDefence()
	{
		return 0;
	}

	short Enemy::checkKO()
	{
		return 0;
	}

	bool Enemy::isAlly(string p_name)
	{
		return false;
	}
}