/**

'Player' -- models characteristics of the player character and what it can do in the game world

**/

#ifndef PLAYER_H
#define PLAYER_H

#include "Actor.h"
//Why does Item need an include rather than just a simple declaration?
//When compiling m_inventory, the compiler will complain that it cannot construct a vector of Items
//because it does not know how much memory space each Item takes!
#include "Item.h"

#include <vector>

//Forward declare Item : needed for inventory

//class Item;
//Also need Enemy class for offerPeace
//class Enemy;
#include "Enemy.h" //?

namespace LeesEngine
{
   class Player : public Actor
   {
       public:
		   Player(string p_name, ActorAttributes p_attributes); //Default-ish constructor
		   Player(string p_name, short p_HP, /*short**/ActorAttributes p_attributes, short p_roomNo, short p_x, short p_y,
			      string p_race, int p_loot, int p_xp, vector<Item> p_inventory, vector<string> p_allies, string p_rank);
		   void move(short p_direction);
		   int getAttackTime();
		   short getAttackAccuracy();
		   short getAttackPower();
		   short getEvasion();
		   short getDefence();
		   short checkKO();
		   bool isAlly(string p_name);
		   short getDV();
		   short getPV();
		   short getBlockChance(int p_attackTime);
		   short getRoom(); //Why is this only in Player, and not in Actor?
		   short acquireItem(Item item); //Changed from 'getItem' because this is too easy to confuse with accessors
		   void setEquipped(string itemName);
		   string showInventory();
		   string showStats();
		   void levelUp(); //I think this might have been put in the UML as returning a short by mistake...?
		   string offerPeace(Enemy& e);
		   string collaborateWith(Player& target);
		   string giveTo(Item item, Player& receiver);
		   string drop(Item item);
		   string getPlayerSaveData();
		   string getRank();
		   string getLastQueryAsked();
		   void setLastQueryAsked(string query);
		   //Test
		   short getCharisma();

       private:
	       string m_race;
		   int m_loot;
		   int m_xp;
		   short m_level;
		   vector<Item> m_inventory;
		   vector<string> m_allies;
		   string m_rank;
		   string m_lastQueryAsked;
   };
}

#endif