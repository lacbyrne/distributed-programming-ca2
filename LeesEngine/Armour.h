/**

'Armour' -- models characteristics of armour in the game that playable characters can wear.
Attributes of armour will influence the flow of battles, amongst... okay, actually nothing else.

**/

#ifndef ARMOUR_H
#define ARMOUR_H

#include "Item.h"

namespace LeesEngine
{
   class Armour : public Item
   {
       public:
		   Armour(string p_name, string p_description, int p_sellValue, string p_use,
						short p_DV, short p_PV, short p_weight, bool p_equipped, string p_equippedOn);
		   short getDV();
		   short getPV();
		   short getWeight();
		   bool isEquipped();
		   string getEquippedOn();

       private:
		   short m_DV;
		   short m_PV;
		   short m_weight;
		   bool m_equipped;
		   string m_equippedOn;
   };
}

#endif