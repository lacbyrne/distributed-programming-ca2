#include "Login.h"

namespace LeesEngine
{
	Login::Login(Connection<Telnet>& p_conn ) : thandler( p_conn )
	{

    }

	void Login::Handle(string p_data)
	{
		string output;

		//Should check here for illegal char.s in name/password...?

		if(m_name == "")
		{
		   //Take in name
		   m_name = p_data;
		   output = "What is your password?";
		}	
		else //password
		{
		   m_password = p_data;
		   //Attempt to log in/create player
		   /*Player p = GameData::loadPlayer(m_name, m_password);*/
		   output = GameData::loginPlayer(m_name, m_password);

		   if(output != Messages::NewPlayer && output != Messages::Login)
		   {
			   //Reset attributes
			   m_name = "";
		       m_password = "";
			   output += newline + "Try again! Name? : ";
		   }
		}

		m_connection->Protocol().SendString(*m_connection, output);

		if(output == Messages::NewPlayer)
		{
		    Player& p = *GameData::loadPlayer(m_name, true);
			//Go to CharacterSetup
			m_connection->AddHandler(new LoggedIn(*m_connection, &p));
		}
		else if(output == Messages::Login)
		{
			Player& p = *GameData::loadPlayer(m_name, false);
			//Go to Game or LoggedIn
		}
	}

	void Login::Enter()
	{
	    m_connection->Protocol().SendString( *m_connection, red + bold + "Welcome To Lee's game, which words alone can't describe!" +
        newline + "What is your name?" + reset );
	}

	void Login::NoRoom(Connection<Telnet>& p_connection)
	{

	}

	void Login::Leave()
	{

	}
    
	void Login::Hungup()
	{

	}

    void Login::Flooded()
	{

	}
}