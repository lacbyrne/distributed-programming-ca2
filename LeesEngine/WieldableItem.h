/**

'WieldableItem' -- models attributes of an item that can be wielded in a hand of a player.
In practice, weapons and shields will be instances of this

**/

#ifndef WIELDABLE_ITEM_H
#define WIELDABLE_ITEM_H

#include "Item.h"

namespace LeesEngine
{
   class WieldableItem : public Item
   {
      public:
		  WieldableItem(string p_name, string p_description, int p_sellValue, string p_use,
			            short p_minPower, short p_maxPower, short p_accuracy, int p_swingTime, bool p_twoHanded,
						short p_DV, short p_PV, bool p_equipped);
		  short getPower();
		  short getAccuracy();
		  int getSwingTime();
		  bool isTwoHanded();
		  short getPV();
		  short getDV();
		  bool isEquipped();

      private:
	      short m_minPower;
		  short m_maxPower;
		  short m_accuracy;
		  int m_swingTime;
		  bool m_twoHanded;
		  short m_PV;
		  short m_DV;
		  bool m_equipped;
   };
}

#endif