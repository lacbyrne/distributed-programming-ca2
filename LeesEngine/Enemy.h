/**

'Enemy' -- models the characteristics and behaviour of enemies in the game

**/

#ifndef ENEMY_H
#define ENEMY_H

#include "Actor.h"

namespace LeesEngine
{
	//Forward declare Room for chooseNextAction function
	class Room;

	class Enemy : public Actor
	{
	    public:
			Enemy(); //Initialising data structure for enemies in Rooms requires a default constructor for enemies
			Enemy(string p_name, short p_HP, /*short**/ActorAttributes p_attributes, short p_roomNo, short p_x, short p_y,
				  int p_xpYield, short p_aggression, string p_ally, string p_specName, short p_specPower, short p_specAcc, short p_specChance,
				  int p_specRecharge, string p_description);
			void move(short p_direction);
			int getAttackTime();
		    short getAttackAccuracy();
		    short getAttackPower();
		    short getEvasion();
		    short getDefence();
		    short checkKO();
		    bool isAlly(string p_name);
			string chooseNextAction(Room* surroundings);
			string getDescription();

	    private:
		    int m_xpYield;
			short m_aggression;
			string m_ally;
			string m_specName;
			short m_specPower;
			short m_specAcc;
			short m_specChance;
			int m_specRecharge;
			string m_description;
	};
}

#endif