/**

'Map' -- a dungeon crawl / MUD game map is in practice a container of the rooms.
As such, it will contain a data structure to store all rooms and functions to manipulate 'em.

**/

// WARNING ABOUT USING THIS APPROACH : 
// Map stores many rooms which each store many players, enemies and items,
// which can end up looking like an enormous array of Enemies and Items...
// Destructing a map seems to take a considerable amount of CPU cycles,
// and creating multiple instances of Map [which won't be done in practice] even causes stack overflow!! >_<
// So how should this be handled...?

#ifndef MAP_H
#define MAP_H

#include "Room.h"

namespace LeesEngine
{
   class Map
   {
       public:
		   //Map(Array<Room> p_rooms); //?
		   Map( bool dummy ); //.... ?????
		   
		   Room getRoom(int index); // ** Testing only!! **
		   void setRoom(Room& data, int index);
		   void spawnEnemy();
		   void loadMap();
		   void saveMap();

       private:
		   Room m_rooms[34]; //? Just assume 34 to be the size for now...
		   //Array<Room> m_rooms; //For now... (Is nothing going to go to Plan??? {*sigh*})
   };
}

#endif