#include "Player.h"

namespace LeesEngine
{
	//Default constructor... or, close to one
	Player::Player(string p_name, ActorAttributes p_attributes)
		: Actor(p_name, 0, p_attributes, 0, 0, 0)
	{
		m_race = "";
		m_loot = 0;
		m_xp = 0;
		m_level = 0;
		m_rank = "Noob";
		m_lastQueryAsked = "";
	}

	Player::Player(string p_name, short p_HP, /*short **/ActorAttributes p_attributes, short p_roomNo, short p_x, short p_y,
			      string p_race, int p_loot, int p_xp, vector<Item> p_inventory, vector<string> p_allies, string p_rank)
				  : Actor(p_name, p_HP, p_attributes, p_roomNo, p_x, p_y)
	{

	}

    void Player::move(short p_direction)
	{
		
	}

	int Player::getAttackTime()
	{
	   return 0;
	}
 
	short Player::getAttackAccuracy()
	{
		return 0;
	}

	short Player::getAttackPower()
	{
		return 0;
	}

	short Player::getEvasion()
	{
		return 0;
	}

	short Player::getDefence()
	{
		return 0;
	}

	short Player::checkKO()
	{
		return 0;
	}

	bool Player::isAlly(string p_name)
	{
		return false;
	}

	short Player::getCharisma()
	{
		return m_attributes.charisma;
	}
}