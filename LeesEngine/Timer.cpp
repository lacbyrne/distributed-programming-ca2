#include "Timer.h"

namespace LeesEngine
{
	Timer::Timer()
	{
		/*m_initSystTime = 0;
		m_initSystTime = calcTimeElapsed();*/
		//Get time passed since system was started
		//this will be taken from future calculations to get time since *game* started
		QueryPerformanceCounter( (LARGE_INTEGER*)&m_totalCounter);
	    QueryPerformanceFrequency( (LARGE_INTEGER*)&m_totalRate);
		m_initSystTime = (m_totalCounter / m_totalRate * 1000);
		calcTimeElapsed();
		m_timeSinceLastUpdate = 0;
		m_oldTime = m_timeElapsed;
	}

	void Timer::calcTimeElapsed()
	{
	    QueryPerformanceCounter( (LARGE_INTEGER*)&m_totalCounter);
	    QueryPerformanceFrequency( (LARGE_INTEGER*)&m_totalRate);	
		m_timeElapsed = (m_totalCounter / m_totalRate * 1000) - m_initSystTime;
		m_timeSinceLastUpdate = m_timeElapsed - m_oldTime;
		m_oldTime = m_timeElapsed;
	}

	largeNum Timer::totalTimePassed()
	{
		return m_timeElapsed;
	}

	bool Timer::timePassedSince(int interval)
	{
		//...???
		return false;
	}
}