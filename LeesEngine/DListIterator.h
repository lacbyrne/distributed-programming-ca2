//Iterator class for a Doubly Linked List

#ifndef DLISTITERATOR_H
#define DLISTITERATOR_H

#include "DListNode.h"

template<class Datatype>
class DListIterator
{
  public:
    //Constructor- takes in a pointer to a DLinkedList and a pointer to a DListNode as parameters
    DListIterator(DLinkedList<Datatype>* list = 0, DListNode<Datatype>* node = 0)
	{
      mList = list;
	  mNode = node;
	  mPosition = 0;
	}

	//Start function- sets the iterator's node to the head node of the list
	void Start()
	{
      if(mList != 0)
	  {
        mNode = mList->mHead; 
		mPosition = 0; //Set position to that of head node- 0
	  }
	}

	//End function- sets the iterator's node to the tail node of the list
	void End()
	{
      if(mList != 0)
	  {
        mNode = mList->mTail;
		mPosition = (mList->mCount)-1; //Set position to that of tail node- the number of nodes in the list minus 1
	  }
	}

	//Forth function- sets the iterator's node to the node after it in the list
	void Forth()
	{
      if(mNode != 0)
	  {
        mNode = mNode->mNext;
	  }
	  mPosition++; //Incremement the iterator's position
	}

	//Back function- sets the iterator's node to the node before it in the list
	void Back()
	{
      if(mNode != 0)
	  {
        mNode = mNode->mPrevious; 
	  }
	  mPosition--; //Decrement the iterator's position
	}

    Datatype Item()
	{
      return mNode->mData;
	}

	//Valid function- tests if a node still 'exists'. 
	//Returns true or false according to whether or 'mNode' points to '0'
	bool Valid()
	{
      return (mNode != 0);
	}

    //Attributes
	DListNode<Datatype>* mNode; //A pointer to a node
	DLinkedList<Datatype>* mList; //A pointer to a list
	int mPosition; //Stores the current position of the iterator; so functions in DLinkedList that call on it can easily work out where it is
};

#endif