#include "LoggedIn.h"

namespace LeesEngine
{
	LoggedIn::LoggedIn(Connection<Telnet>& p_conn, Player* p_player ) : thandler( p_conn )
	{
		m_player = p_player;
    }

	void LoggedIn::Handle(string p_data)
	{
		//Might need to redraw menu here from time to time, depending on inputs...?
		string output = "straws";

		output = m_player->getName();
		GameData::debugActivePlayers();

		m_connection->Protocol().SendString(*m_connection, output);
	}

	void LoggedIn::Enter()
	{
		m_connection->Protocol().SendString(*m_connection, m_player->getName());
		loggedInMenu();
	}

	void LoggedIn::Leave()
	{

	}
    
	void LoggedIn::Hungup()
	{

	}

    void LoggedIn::Flooded()
	{

	}

	void LoggedIn::loggedInMenu()
	{
	   m_connection->Protocol().SendString(*m_connection, newline + yellow + "[]Enter the game " + newline
		                                   + "[]View the highscores" + newline
										   + "[]View your logs" + newline
										   + "[]Roll a new character" + newline
										   + "[]Exit the game? " + reset);
	}
}