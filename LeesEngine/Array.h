//A nice dynamic array

#ifndef ARRAY_H
#define ARRAY_H

#include <iostream>

using namespace std;

template<class Datatype>
class Array
{
  public:
    //Constructor
    Array(int size, int growSize) 
	{
      mArray = new Datatype[size]; //Constructs a new dynamic array- using the 'new' method of C++-  of specifed size
	  mSize = size; 
	  mGrowSize = growSize;
	  mNumElements = 0; 
	}
    
	//Destructor method: deletes the Array object
	~Array()
	{
      if(mArray != 0) 
	  {
        delete[] mArray;
	  }
	  mArray = 0;
	}

	//Overload '[]' operator to make it return refernence to the datatype in the appropriate cell
	Datatype& operator[](int index)
	{
      return mArray[index];
	}

	//Resize algorithm
	void Resize(int size)
	{
      Datatype* newArray = new Datatype[size]; //Create a new array of the size that is required
	  if(newArray == 0)
	  {
        return;
	  }

	  //Calculate min- which will store the value of whichever is lower between the current size of mArray and its new size
	  int min;
	  if(size < mSize) 
	  {
        min = size;
	  }
	  else
	  {
        min = mSize;
	  }

	  int index;
	  //Use a loop to copy each element in turn from mArray [the old index] to the new array. 
	  //Number of iterations is determined by the variable 'min', which was explained above
	  for(index = 0; index < min; index++) 
	  {
        newArray[index] = mArray[index];
	  }

	  mSize = size; //Set mSize to new size of the array
	  if(mArray != 0)
	  {
        delete[] mArray;
	  }
	  
	  mArray = newArray; //Replace mArray with the new array
	  newArray = 0;
	}

    //Push function for inserting a new element
	void Push(Datatype input)
	{
	  if(mNumElements == mSize)
	  {
        //The array must be resized to allow more elements to be added
		int newSize = mSize + mGrowSize;
		Resize(newSize);
	  }
	  mArray[mNumElements] = input;
	  mNumElements++;
	}

	//Inserts an element at a particular poisition in the array
	void Insert(Datatype input, int index)
	{
      int tempIndex;
	  for(tempIndex = mSize-1; tempIndex > index; tempIndex--)
	  {
        mArray[tempIndex] = mArray[tempIndex-1];
	  }

	  mArray[index] = input;
	  mNumElements++;

	  cout << mNumElements;
	}

	//...? =P
	void Replace(Datatype input, int index)
	{
	   //Datatype bob(input);

	   mArray[index] = input;
	   //mArray[index] = bob;
	}

	//Remove the element at a particular position in the array
	void Remove(int index)
	{
      short tempIndex;

      for(tempIndex = index+1; tempIndex < mNumElements; tempIndex++ )
	  {
        mArray[tempIndex - 1] = mArray[tempIndex];
	  }
	  
	  if(mNumElements > 0)
	  {
	    mNumElements--; //Decremement the number of elements in the array
	  }
	}

	//Pop function for ridding of the last element in the array
	void Pop()
	{
      mNumElements--; 
	}

	//Clear function- resets number of elements in stored in the array
	void Clear()
	{
	  mNumElements = 0;
	}

	//Count function for returning the number of elements in the array
	int Count()
	{
      return mNumElements;
	}

	int Capacity()
	{
      return mSize;
	}

  private:
    Datatype* mArray; //The actual array at the heart of the OrderedArray class
    int mSize; //The current capacity of the array- i.e. how much data it CAN store. [mSize can be greater than mNumElements, but not vice versa]
	int mGrowSize; //Specifies by how much to increase the capacity of the array when it needs to be resized
	//The number of *elements* currently in the array- as opposed to the amount of data 'cells' / indices it contains. 
	//The functions for sorting and searching for data is only concerned with the number of elements in the array; 
	//anything in the array beyond the number of elements would be empty spaces.
	int mNumElements; 
};

#endif