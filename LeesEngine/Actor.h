/**

'Actor' -- abstract class that provides common attributes and functions to playable characters and enemies.

**/

#ifndef ACTOR_H
#define ACTOR_H

#include <string>

using namespace std;

namespace LeesEngine
{
   struct ActorAttributes
   {
	   short strength;
	   short endurance;
	   short dexterity;
	   short agility;
	   short intelligence;
	   short spirit;
	   short charisma;
   };

   class Actor
   {
      public:
		/*inline string getName() {return m_name;}*/
		string getName();
		inline short getHP() { return m_HP;}
		inline void setHP(short p_HP) {m_HP = p_HP;}
		virtual void move(short p_direction) {};
		string attack(Actor& target);
		string specialAttack(Actor& target, short specAcc, short specPower, short specRecharge);
		virtual int getAttackTime() {return 0;}
		virtual short getAttackAccuracy() {return 0;}
		virtual short getAttackPower() {return 0;}
		virtual short getEvasion() {return 0;}
		virtual short getDefence() {return 0;}
		virtual short checkKO() {return 0;}
		virtual bool isAlly(string p_name) {return false;}

      protected:
		Actor(); //A default constructor for Enemy will require a default constructor for Actor... =P
		//Actor(string p_name, short p_HP, short* p_attributes, short p_roomNo, short p_x, short p_y);
		Actor(string p_name, short p_HP, /*short**/ActorAttributes p_attributes, short p_roomNo, short p_x, short p_y);
	    string m_name;
		short m_HP;
		//short m_attributes[7]; //strength, endurance, dexterity, agility, intelligence, spirit, charisma
		ActorAttributes m_attributes;
		short m_roomNo;
		short m_x;
		short m_y;
   };
}

#endif ACTOR_H