#include "GameData.h"

namespace LeesEngine
{
	/*static vector<Item> m_itemTypes;
    static vector<Enemy> m_enemyTypes;
    static Map m_map;*/
	vector<PlayerData> GameData::m_playersLogins;
	vector<Player> GameData::m_activePlayers;

	void GameData::setupData()
	{
		loadPlayerLogins();
		loadEnemies();
		loadItems();
		//**For now : sticking in garbage for the first space in m_activePlayers (since bad things happen if C++ attempts to return ref to nothing)**
		ActorAttributes temp = {0,0,0,0,0,0,0};
		Player p(" ", temp);
		m_activePlayers.push_back(p);
	}

	void GameData::loadPlayerLogins()
	{
		ifstream in("playerlogins.txt");

		if(in)
		{
			string dataEater = "";
			string name = "";
			string password = "";
			while(in.good() )
			{
				getline(in, dataEater);
				name = dataEater.substr(0, dataEater.find(','));
				password = dataEater.substr(dataEater.find(',')+2, dataEater.length() );
				PlayerData pd = {name, password, false};
				m_playersLogins.push_back(pd);
			}
		}

		in.close();
	}

	void GameData::loadItems()
	{

	}

	void GameData::loadEnemies()
	{

	}

	//Check if player is to logged in or created
	string GameData::loginPlayer(string name, string password)
	{
		bool bFound = false;
		vector<PlayerData>::iterator ITER;
		for(ITER = m_playersLogins.begin(); ITER != m_playersLogins.end(); ++ITER)
        {
			if(ITER->name == name)
			{
				//Check password
				if(ITER->password == password && (!ITER->loggedin) )
				{
					ITER->loggedin = true;
					return Messages::Login;
				}
				else if(ITER->password != password)
				{
					//return m_activePlayers.front();
					return Messages::WrongPassword;
					cout << ITER->password;
				}
				else
				{
					return Messages::AlreadyLoggedin;
				}
			}
        }

		if(!bFound)
		{
		   //Add data to playerData
		   PlayerData newData = {name, password, true};
		   m_playersLogins.push_back(newData);
		   //Save playerData
		   ofstream out("playerlogins.txt", ios_base::app); //Set write stream to append -- not overwrite!
		   if(out)
		   {
		      out << "\n" << name << ", " << password << "";
		      //Create new player
		   }
		   out.flush();

		   return Messages::NewPlayer;
		}

		return "?";
	}

	//Player& GameData::loadPlayer(string name, bool bNew)
	vector<Player>::iterator GameData::loadPlayer(string name, bool bNew)
	{
		if(bNew) //Must be new here...
		{
			//Create default player to be customised
			ActorAttributes attributes = {0,0,0,0,0,0,0};
			Player p(name, attributes);
			m_activePlayers.push_back(p);
			return getPlayer(name);
		}
		else
		{
			//Load data for that player...
			/*string fileName = name + ".txt";
			ifstream in(fileName.c_str() );

			if(in)
			{
			    //Load 'em up!... when this sort of stuff gets implemented.
				Player p(name);
				m_activePlayers.push_back(p);
				return p;
			}*/

		}

		//else
		//return m_activePlayers.front();
		return getPlayer(" ");
	}

	//Give player reference to player character in m_activePlayers
	//Ideally this will only be done once for each player, when they log on...
	//This won't be used everytime a player takes an action -- their connection uses its own reference
	//Player& GameData::getPlayer(string name)
	vector<Player>::iterator GameData::getPlayer(string name)
	{
		vector<Player>::iterator ITER;

		for(ITER = m_activePlayers.begin(); ITER != m_activePlayers.end(); ++ITER)
        {
			if(ITER->getName() == name)
			{
				return ITER;
			}
		}

		return ITER;
	}

	//Test
	void GameData::debugActivePlayers()
	{
		vector<Player>::iterator ITER;

		for(ITER = m_activePlayers.begin(); ITER != m_activePlayers.end(); ++ITER)
        {
			cout << ITER->getName() << endl;
		}
	}
}