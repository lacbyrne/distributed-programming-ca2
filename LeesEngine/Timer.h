/**

'Timer' -- a customised solution to tracking game time as a substitute for Ron Penton's Basic Library

**/

#ifndef TIMER_H
#define TIMER_H

#include <Windows.h>

namespace LeesEngine
{
	typedef long long int largeNum;

    class Timer
	{
	    public:
			Timer();
			largeNum totalTimePassed();
			bool timePassedSince(int interval);

	    private:

			void calcTimeElapsed();

			largeNum m_timeElapsed;
			largeNum m_timeSinceLastUpdate;
			//Other variables that might be needed in calculating the primary two...?
			largeNum m_initSystTime;
			largeNum m_totalCounter;
			largeNum m_totalRate;
			largeNum m_oldTime;
	};
}

#endif