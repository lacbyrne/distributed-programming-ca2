#include <string>

#include "../SocketLib/SocketLib.h"
#include "Player.h"
#include "GameData.h"
//#include "Logs.h"

using std::string;
using namespace SocketLib;

namespace LeesEngine
{
    #ifndef LOGGEDIN_H
    #define LOGGEDIN_H

    class LoggedIn : public Telnet::handler
	{
		typedef Telnet::handler thandler;

	    public:
		   LoggedIn( Connection<Telnet>& p_conn, Player* p_player);
		   void Handle(string p_data);
		   void Enter();
		   void Leave();
		   void Hungup();
		   void Flooded();

	     private:
            Player* m_player;
			void loggedInMenu(); //Display text for the menu for this handler
	};

    #endif
}