//++Ordered array class++
//Uses a template that passes in two parameters- 'Datatype' of type class, which allows the user to specify what datatype they will be working with; and 'zero' of type Datatype, which holds the value entered for representing the value of an element that has been deleted [e.g. ints might use '0', chars might use '-'...]
template<class Datatype, Datatype zero>
class LeeArray
{
  public:
    //Constructor method: Creates a new array of the datatype specified by the user.
	//Takes in parameters for size of the array to be constructed; and the amount the user requests that the array should grow by when it needs to grow [i.e. if another element has to put in it when it's already full] 
    LeeArray(int size, int growSize) 
	{
      mArray = new Datatype[size]; //Constructs a new dynamic array- using the 'new' method of C++-  of specifed size
	  
	  mSize = size; //Record the size entered for the array in the member variable mSize
	  mGrowSize = growSize; //Record the growSize entered in the member variable mGrowSize
	  mNumElements = 0; //When the array is constructed, there are no elements- so mNumElements = 0
	}
    
	//Destructor method: deletes the LeeArray object. No return type or parameters.
	~LeeArray()
	{
      if(mArray != 0) //If mArray has not been set to 0 [which is done to indicate an array no longer in use], call the 'delete' function to delete it
	  {
        delete[] mArray;
	  }

	  mArray = 0; //Set mArray to 0 to indicate it no longer in use
	}

	//The standard Resize Algorithm for a dynamic array. Needs to be implemented in this LeeArray class as the array will need to be resized if another element is to be Pushed into the array when it is already full
	void Resize(int size)
	{
      Datatype* newArray = new Datatype[size]; //Create a new array of the size that is required
	  if(newArray == 0) //Cease running function if the new array is invalid
	  {
        return;
	  }

	  //Calculate min- which will store the value of whichever is lower between the current size of mArray and its new size
	  int min;
	  if(size < mSize) 
	  {
        min = size;
	  }
	  else
	  {
        min = mSize;
	  }

	  int index;
	  for(index = 0; index < min; index++) //Use a loop to copy each element in turn from mArray [the old index] to the new array. Number of iterations is determined by the variable 'min', which was explained above
	  {
        newArray[index] = mArray[index];
	  }

	  //Replace mArray with the new array, and rid of newArray
	  mSize = size; //Set mSize to new size of the array
	  if(mArray != 0)
	  {
        delete[] mArray;
	  }
	  
	  mArray = newArray; //Replace mArray with the new array
	  newArray = 0; //Set newArray to 0
	}

	//'Push' function: used to add a new element to the array. Takes in one parameter- that is the element to be added to the array.
	//Since the LeeArray class is to determine the order of its elements at insertion, the element must be put in the correct position relative to the elements in the array. 
	void Push(Datatype input)
	{
	  if(mNumElements == mSize) //If mNumElements == mSize, that means the array is full of elements
	  {
        //The array must be resized to allow more elements to be added
		int newSize = mSize + mGrowSize; //New size of the array is going to be its original size PLUS the amount specified for the user that it will be resized by if it needs to
		Resize(newSize); //Call the Resize function to resize the array
	  }

	  //The order of elements in the LeeArray is determined when an element is inserted; so I must implement algorithms to determine which position to put the new element in relative to the existing elements in the array
      //The most efficient way to locate the index/'cell' that the element should be put into in to do a BinarySearch.
	  //However, this would be inefficient and practically impossible to do for array with less than 3 elements.
    
	  if(mNumElements == 0) //If there are no elements in the array, the element can go straight into the first cell
	  {
        mArray[0] = input;
	  }
	  else if(mNumElements == 1) //One element in the array- check if the element to place is smaller or larger than the existing element
	  {
        if(input < mArray[0]) //Input is smaller than than the first cell
		{
		  //Move existing element into second cell, and place the new element in the first cell
          mArray[1] = mArray[0]; 
		  mArray[0] = input;
		}
		else if(input >= mArray[0]) //Input is larger than or equal to that at the first cell
		{
		  //Existing element can stay where it is; new element enters the second cell
		  mArray[1] = input;
		}
	  }
	  else if(mNumElements == 2) //2 existing elements in the array
	  {
	    if((input > mArray[0]) && (input <= mArray[1])) //Input is larger than first element and smaller than second element
		{
		  //Move the bigger element to the third cell. The new element will take its place in the second cell
          mArray[2] = mArray[1]; 
		  mArray[1] = input; 
		}
        else if(input < mArray[0]) //Input is smaller than first element		
		{
		  //Move both the existing elements up by one cell, and put the new element in the first cell in the array
          mArray[2] = mArray[1];
		  mArray[1] = mArray[0];
		  mArray[0] = input;
		}
		else if(input >= mArray[1]) //Input is larger than or equal to second element
		{
          //New element can thus go straight into the last cell
          mArray[2] = input;
		}
	  }

	  else if (mNumElements > 2) //3 or more elements already in array- binary search may be used
	  {
    	//A binary search for the index to place the new element in will be executed. This usually proves more efficient than doing a linear search for the correct index to put the element in. For particularly large arrays, binary searching can dramatically reduce the time taken to find any particular element in the array that cannot be specified prior to the search
	    //Exact operation of the binary search is explained in the 'Search' function

		int hi = mNumElements; //Set 'hi' [comes directly after the highest index to inspect] to the number of elements in the array
	    int lo = 0; //Set 'lo' [lowest index to inspect] to that of first element
	    int posToCheck = (int)(((hi+lo)/2)); //Get position to check [a.k.a. 'mid'] by dividing sum of hi and lo by 2
		//MAKE SURE TO ROUND UP THE RESULT BY TYPECASTING IT AS AN INT! [No such as 'half a cell', after all...]

		/*Loop while 1/'posToCheck' has not reached its potential minimum [lo] or maximum [hi]
		             2/'posToCheck' does not equal the first index
					 3/'posToCheck' does not refer to last index to contain an element [The last two conditions are dealt with AFTER the loop*/
	    while(((lo < posToCheck) && (hi > posToCheck)) && (posToCheck > 0) && (posToCheck < mNumElements-1))
	    {
		  //If the value of the input element is both smaller than that in the index before, and larger than OR EQUAL TO that in the index after it, this *might* be the correct position to put the element in.
	      //[Note that if the 'equal to' condition is omitted, trying to add a duplicate of an existing element into the array might cause the program to become stuck in a never-ending loop]
	      if((input < mArray[posToCheck+1]) && (input >= mArray[posToCheck-1])) 
		  {
			//HOWEVER!: Just because the number is smaller than the one on the right and larger than the one on the left, doesn't mean it's simultaneously larger than *every* element before that position and smaller than every element after that position. 
			//The element that is already in the index that the input element is to be placed in should also be checked to see whether this is larger, smaller or equal to the input element
            
			if(mArray[posToCheck] < input) //If the existing element in the possible position for the new element is smaller than it 
			{
			  //The new element should be inserted *after* the element already in this position. 
			  //So increase the original number found for the posToCheck [the position *found* for the new element] by one.
              posToCheck++;
			}
			//The same result could have been achieved by checking the element already in the index indicated by 'posToCheck' to see if it larger than the element to be placed- and if so, decreasing the index to place the element into by one.

		    break; //Found the right index to put the element into- so exit the loop
		  }
		  //First condition was false; so check that the element is still smaller than the one in the index after it. 
		  //If so, reconfigure the highest index to check and the next position to check
		  else if(input < mArray[posToCheck+1]) 
		  {
            hi = posToCheck;
		    posToCheck = (int)(hi + lo)/2;
		  }
		  //Failing that, check that the element is still bigger than or equal to the first than the one in the index before it. 
		  //If so, reconfigure smallest index to check and next position to check.
		  else if(input >= mArray[posToCheck-1]) 
		  {
            lo = posToCheck;
		    posToCheck = (int)(hi + lo)/2;
	   	  }
        }; //End loop

		//If the loop was ended without the correct index/position in the array being located, 
		//it's likely that the value for 'posToCheck' could no longer satisfy the conditions of the while loop because there was either no index before or no index after. 
		//[These conditions are in place as the first condition in the loop checks indices before and after the index indicated by the variable; so if the variable referred to an index at either 'end' of the array, 'fencepost error' would occur]
	    
		if(input < mArray[0]) //Input element is smaller than that at the first index
	    {
          posToCheck = 0; //Set final value for 'posToCheck' equal to the first index 
	    }
	    else if(input >= mArray[mNumElements-1]) //Input element is larger or equal to that at the last index
	    {
          posToCheck = mNumElements; //Set final value for 'posToCheck' to that of index after that of the last element 
		  //[This definitely exists; because if there weren't enough empty spaces when this function was called, then the array was resized]
	    }
		
		//Now all elements after the index that the input element was placed in must be moved an index higher. [These will all be bigger than the value of the input element anyway]
		for(int i = (mNumElements-1); i >= posToCheck; i--) //Loop starts at the last element, and goes backwards until the element index that the one the new element will be placed in. This way, no cells are over-written
	    {
          mArray[i+1] = mArray[i];
	    }
	  
		mArray[posToCheck] = input; //Finally! Put the new element in the proper cell that was found using the above algorithm
	  }

	  mNumElements++; //New element has been added to the array- so increment mNumElements
	}

	//'Pop' function: locates the last element in the array, and removes it by setting the value in its index to that of the template parameter for 'zero' [bearing in mind that using the value '0' is not robust, as it assumes an int short or long]
	void Pop()
	{
	  //Since mNumElements keeps track of the number of elements [occupied data cells, as opposed to total data cells] in the array, 
	  //The last actual element in the array is at [mNumElements-1]. Set this to the value provided for 'zero'
	  mArray[mNumElements-1] = zero; 
	  mNumElements--; //Decremement number of elements in the array
	}

	//'Remove()' function: Removes an element from the array at a particular index. Takes in one argument- 'index', of type int, that represents the index in the array to remove an element from. 
	void Remove(int index)
	{
      //Unless the element to remove is the last one in the array, it does not necessarily have to be set to 'zero'. Its value will be overriden by that of the element after it, when all elements that have higher values than the removed element will be moved 'back' an index to keep the order
	  if(index == (mNumElements-1))
	  {
        mArray[index] = zero;
	  }
	  else
	  { 
	    //The element is to be removed *with the order still intact*. 
		//So- starting at the element after the index to remove, and ending at the last element- move all elements larger (or equal to?) than the removed element to the array index one lower than them
	    for(int i = index+1; i < mNumElements; i++)
		{
          mArray[i] = mArray[i+1];
		}
	  }
	  mNumElements--; //Decremement the number of elements in the array
	}

    //'Search' function- takes in an element to search for as a parameter, searches for that element in the array, and returns the index of the element returned
	//The function contains both a linear search method for small arrays, and a binary search method for larger arrays.
	int Search(Datatype key)
	{
      //Using a linear search to find the element if the array size is, say, 5 or less.
	  if(mNumElements <= 5)
	  {
        //Linear Search: simply loop through the array, check if the element at that index matches the element to search for, and return its index. If the element is not found, return -1.
	    for(int i = 0; i < mNumElements; i++)
		{
          if(mArray[i] == key) //Found the element to search for in the array- return its index
		  {
		    cout << key << " located at position " << i << endl;
            return i;
		  }
		}
	  }
	  else 
	  {
        //Binary Search- looks at middle element in the array, checks to see if it equals the element to search for. 
		//If not, checks whether the element at this index is higher or lower than the element to search for. If smaller, the middle element between this and the first element [or other agreed minimum index] is searched; if larger, middle element between this and the last element [or other agreed maximum index] is searched. 
		//This repeats until the element is found, or all possible indices that the element COULD be located in have been checked and it hasn't be found [i.e. it isn't there]
	    
		int hi = mNumElements; //Set highest index to be the number of elements in the array [it would be incorrect to set this to the last index occupied by an element- i.e. (mNumElements-1) - as if the max was set to this then not all elements in the array would be encompassed. Thus, 'hi' must to be set to be set to a value that is one higher than that of the index of the final element in the array
		int lo = 0; //Set lowest index to be the first index in the array
		int mid = (int)((hi+lo)/2); //**The middle index is found with the formula (hi+lo)/2 MAKING SURE TO ROUND UP THE RESULT BY TYPECASTING IT AS AN INT**

		//Keep the binary search loop going while 'mid' does not equal or go under the value set for the lowest index; and 'mid' does not equal or exceed the value set for the highest index. 
		//If either of these conditions were allowed to happen in the Search loop without being avoided, the formula used for determining which index to check would cause the loop to continue never-endingly
		while((lo < mid) && (hi > mid)) 
		{
          if(mArray[mid] == key) //The element at the middle index that was found using the formula equals the element being searched for
		  {
		    //The requested element has been found; BUT the search function should ideally return the FIRST instance of that element in the array [if there are duplicates]. 
			//Since this is an ordered array, that can be located by a linear search going backwards from the located index whilst the element before the element being inspected is equal to it
		    
			if(mid != 0) //Before doing this, make sure the index found by the search is not the first one- not only will the first index definitely contain the first instance of the requested number, but attempting to check the index before it will result in 'fencepost error'
			{
			  //While the element that was found by the search is equal to the one before it, decrement 'mid'
		      //[after successfully completing the binary search, it is the value of 'mid' that will be returned from the function; so the idea to have it set to indicate the first instance of the element in the array]
              while(mArray[mid] == mArray[mid-1]) 
			  {
                mid--; 
				//Seeing as it is still possible that mid could reach 0 while searching backwards through the array, make sure to terminate the loop if this happens!
				if(mid == 0)
				{
                  break;
				}
			  };
			}
			//Found the right position and done checking for duplicates- so return the index that the element was found it
            cout << key << " located at position " << mid << endl;
			return mid;
		  }

		  else if(mArray[mid] > key) //Element at middle index is larger than the element being searched for
		  {
            //Need to search in the array indices AFTER the original middle index found- 
			//so set the new minimum index to that of the index that was searched, and use the formula to calculate the next index to search
		    hi = mid;
			mid = (int)((hi+lo)/2);
		  }
		  else if(mArray[mid] < key) //Element at middle index is smaller than the element being searched for
		  {
            //Need to search in the indices BEFORE the original middle index found- 
			//so set new maximum index to that of the index that was searched, and calculate the next index to search
		    lo = mid;
			mid = (int)((hi+lo)/2);
		  }
		}; //end of binary search loop
	  }

	  //Either the linear search loop didn't the element to search for; or the binary search loop was terminated without finding anything
	  //So return from the method unsuccessful
	  cout << key << " could not be located in the array!" << endl;
	  return -1;
	}

	//'Clear' function: empties the array by setting all elements to 'zero' [the template argument relevant to the array's datatype], and resetting the number of current elements in the array to 0 [so that the emptied cells can be replaced by new values]
	void Clear()
	{
	  for(int i = 0; i < mNumElements; i++) //Cycle through the elements in the array and set them all to 'zero'
	  {
        mArray[i] = zero;
	  }
	  mNumElements = 0; //Array is emptied, so mNumElements should be reset to 0
	}

  private:
    Datatype* mArray; //The actual array at the heart of the LeeArray class
    int mSize; //The current capacity of the array- i.e. how much data it CAN store. [mSize can be greater than mNumElements, but not vice versa]
	int mGrowSize; //Specifies by how much to increase the capacity of the array when it needs to be resized
	int mNumElements; //The number of *elements* currently in the array- as opposed to the amount of data 'cells' / indices it contains. The functions for sorting and searching for data is only concerned with the number of elements in the array; anything in the array beyond the number of elements would be empty spaces.
};