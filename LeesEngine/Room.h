/**

'Room' -- models characteristics of a room in the game.
It is currently proposed that the room would be a complex structure
that would allow enemies and players to walk around within the room,
and the room would have to track the players, enemies and items in the room, their states and positions.

**/

#ifndef ROOM_H
#define ROOM_H

//#include "HashTable.h"
#include "Array.h"
#include "Enemy.h"
#include "Item.h"

#include <string>

using namespace std;

//class Actor;
//class Enemy;
//class Item;

namespace LeesEngine
{
   struct Exits
   {
      short north;
	  short south;
	  short west;
	  short east;
   };

   class Room
   {
       public:
		   Room(); //Initialising Room array in Map requires a default constructor for rooms

		   /*~Room()
		   {
		      m_players.~Array();
		      m_enemies.~Array();
		      m_items.~Array();
		   }*/

		   Room(short p_roomNo, string p_roomType, short p_dangerLevel, 
			    /*Array<string> p_players, Array<Enemy> p_enemies, Array<Item> p_items,*/
				Exits p_exits, string p_description);
		   //Room(short p_roomNo, string p_roomType, short p_dangerLevel, Exits p_exits, string p_description);
		   Room(string p_description);
		   string drawRoom();
		   string isSpaceOccupiable(short x, short y);
		   Actor& findTargetOn(short x, short y);
		   Item getItemOn(short x, short y);
		   string getDescription();
		   void spawnEnemy();
		   void addItem(Item item);
		   //...
		   Item getItem(int index);

       private:
		   short m_roomNo;
		   string m_roomType;
		   short m_dangerLevel;
		   //Values are hashed, stored and retrieved, based on their positions
		   /*HashTable<short, string> m_players;
		   HashTable<short, Enemy> m_enemies;
		   HashTable<short, Item> m_items;*/
		   //\-->Are hash tables worth it, to store 25 bits of data per structure??
		   //Will try with dynamic arrays instead...
		   /*Array<string> m_players;
		   Array<Enemy> m_enemies;
		   Array<Item> m_items;*/
		   string m_players[25];
		   Enemy m_enemies[25];
		   Item m_items[25];
		   //short[4] m_exits;
		   Exits m_exits;
		   string m_description;
   };
}

#endif