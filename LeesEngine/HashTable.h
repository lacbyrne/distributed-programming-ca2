//This file holds the Linked Hash Table implementation

#ifndef HASHTABLE_H
#define HASHTABLE_H

#include "DLinkedList.h"
#include "Array.h"

/*-------------------------------------------------------------------------------
  Name:       HashEntry
  Description: This is the hash table entry class. It stores a key and data pair.
  -------------------------------------------------------------------------------*/
template<class Keytype, class Datatype>
class HashEntry
{
  public:
	Keytype mKey;
	Datatype mData;
};

/*----------------------------------------
  Name:      HashTable
  Description: This the hash table class
  ----------------------------------------*/
template <class Keytype, class Datatype>
class HashTable
{
  public:
    //typedef the entry class...
    typedef HashEntry<Keytype, Datatype> Entry;

    /*--------------------------------------------------------------------------------------------
	  Name:        HashTable
	  Description: The constructor. Constructs the table with a certain size, and a hash function.
	               Will construct the mTable with the correct size.
      Arguments:   size: Size of the table
	               hash: The hashing function
      Return Value: n/a- Constructor function
      --------------------------------------------------------------------------------------------*/
	HashTable(int size, unsigned long int(*hash)(Keytype)) : mTable(size, 6) //Note second parameter isn't explicitly an attribute; it's calling on a function and getting the returned value of the function
	{
      //set the size, hash function and count
	  mSize = size;
	  mHash = hash;
	  mCount = 0;
	}

	/*------------------------------------------------------
	  Name:         Insert
	  Description:  Insert a new Keydata pair into the table
	  Arguments: key: the key
	             data: the data
	  Return Value: none
	  -------------------------------------------------------*/
	void Insert(Keytype key, Datatype data)
	{
      //create an entry
	  Entry entry;
	  entry.mKey = key;
	  entry.mData = data;

	  //get the hash value for the key, and modulo it so that it fits in the table
	  int index = mHash(key)%mSize;

	  //add the entry at the correct index. Increment count
	  mTable[index].Append(entry);
	  mCount++;
	}

	/*----------------------------------------------------------
	  Name:         Find
	  Description:  Finds a key in the table.
	  Arguments: key: the key to search for
	  Return Value: A pointer to the entry that has the key/data
	                -->Or '0', if not found
	  ----------------------------------------------------------*/
    Entry* Find(Keytype key)
	{
      //find the index where the key should exist
	  int index = mHash(key)%mSize;

	  //get an iterator for the list at that index
	  DListIterator<Entry> itr = mTable[index].GetIterator();

	  //search each item
	  while(itr.Valid())
	  {
        //if the keys match, then return a pointer to the entry
	    if(itr.Item().mKey == key)
		{
		  //cout << "Match!";
          return &(itr.Item());
		}
		itr.Forth();
	  }

	  //the key wasn't found! -->Return 0
	  return 0;
	}
	/*------------------------------------------------------
	  Name:         Remove
	  Description:  Remove an entry based on a key
	  Arguments: key: the key to remove
	  Return Value: true if removed;
	                false if it couldn't be found
	  -------------------------------------------------------*/
    bool Remove(Keytype key)
	{
       //find the index where the key *should* be 
	   int index = mHash(key)%mSize;

	   //get an iterator for the list at that index
	   DListIterator<Entry> itr = mTable[index];

	   //search each item
	   while(itr.Valid())
	   {
         //if the keys match, remove the node and return true
	     if(itr.Item().mKey == key)
	 	 {
           mTable[index].Remove(itr);
		   mCount--;
		   return true;
		 }
		 itr.Forth();
	   }

	   //item was not found
	   return false;
	}
 
	//No loooooooooong comment this time- this function is pretty straight forward...
	//Accessor function for the attribute 'mCount'
	int Count()
	{
      return mCount;
	}

	int mSize;
	int mCount;
	Array<DLinkedList<Entry>> mTable;
	unsigned long int (*mHash)(Keytype);
};

#endif