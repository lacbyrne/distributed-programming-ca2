/**

'Item' -- Models characteristics of any sort of item in the game.
Not abstract -- items that are not otherwise specially categorised, like tools and valuable items,
will/should be instances of Items.

**/

#ifndef ITEM_H
#define ITEM_H

#include <string>

using namespace std;

namespace LeesEngine
{
   class Item
   {
      public:
		  Item(); //Data structure for Items in Rooms requires a default constructor for Item
		  string getDescription();

      protected:
		  Item(string p_name, string p_description, int p_sellValue, string p_use);
		  string getName();
		  //string getDescription();
		  int getSellValue();
		  string getUse();

      private:
	      string m_name;
		  string m_description;
		  int m_sellValue;
		  string m_use;
   };
}

#endif