/**

'GameData' -- retrieves, stores and saves volatile data for the playing session.
Also stores the data for the attributes used to construct particular instances of items and enemies,
like halberds, plate armours, minotaurs, berserk behemoths, 
Members so they can be easily available to other classes.

**/

#ifndef GAME_DATA_H
#define GAME_DATA_H

#include "Map.h"
#include "Player.h"
#include "../Messages.h"

#include <vector>
#include <fstream>

//class Player;

namespace LeesEngine
{
   struct PlayerData
   {
      string name;
	  string password;
	  bool loggedin;
   };

   class GameData
   {
       public:
	       static void setupData();
		   static void loadItems();
		   static void loadEnemies();
		   static void loadPlayerLogins();
		   static void loadMap();
		   //static Player& loadPlayer(string name, bool bNew);
		   static vector<Player>::iterator loadPlayer(string name, bool bNew);
		   static void saveMap();
		   static void savePlayers();
		   //Added for feedback back to player whilst attempting to load a character
		   static string loginPlayer(string name, string password);
		   //static Player& getPlayer(string name);
		   static vector<Player>::iterator getPlayer(string name);
		   static void debugActivePlayers();

       private:
		   static vector<Item> m_itemTypes;
		   static vector<Enemy> m_enemyTypes;
		   static Map m_map;
		   static vector<PlayerData> m_playersLogins;
		   static vector<Player> m_activePlayers;
   };
}

#endif