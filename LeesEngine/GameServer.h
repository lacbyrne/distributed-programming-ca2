//Runs the ListeningManager and ConnectionManager for the MUD/game

#include "../SocketLib/SocketLib.h"
#include "../LeesEngine/Login.h"

using namespace SocketLib; //*** DON'T FORGET THE NAMESPACES!! {*sigh*}

namespace LeesEngine
{
    class GameServer
	{
	   public:
		   GameServer(short* ports, short numOfPorts); //Passes in list of ports to listen on
		   void runServer();

	   private:
		   ListeningManager<Telnet, Login> m_lm;
           ConnectionManager<Telnet, Login> m_cm;
	};
}