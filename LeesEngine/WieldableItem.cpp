#include "WieldableItem.h"

namespace LeesEngine
{
	WieldableItem::WieldableItem(string p_name, string p_description, int p_sellValue, string p_use,
			            short p_minPower, short p_maxPower, short p_accuracy, int p_swingTime, bool p_twoHanded,
						short p_DV, short p_PV, bool p_equipped)
	: Item(p_name, p_description, p_sellValue, p_use)
	{

	}
}