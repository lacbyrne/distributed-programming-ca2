#include "Item.h"

namespace LeesEngine
{
	Item::Item()
	{
		m_name = "";
		m_description = "It's an item";
		m_sellValue = 0;
		m_use = "";
	}

	Item::Item(string p_name, string p_description, int p_sellValue, string p_use)
	{
		m_name = p_name;
		m_description = p_description;
		m_sellValue = p_sellValue;
		m_use = p_use;
	}

	string Item::getDescription()
	{
		return m_description;
	}
}