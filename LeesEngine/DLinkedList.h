//This is the header file that defines the Doubly Linked List class
//- a modified Singly Linked List that contains both a pointer to the next node in the list and the previous node

#ifndef DLINKEDLIST_H
#define DLINKEDLIST_H

//Class needs references to DListNode, DListIterator. So these must be forward declared.
//template<class Datatype> class DListNode;
template<class Datatype> class DLinkedList;
//template<class Datatype> class DListIterator;

//Class also needs a reference to an Arrays class- as it needs to store data from searches in a dynamic array
#include "Array.h"
#include "DListNode.h"
#include "DListIterator.h"

#include <string>

using std::string;

//+++++++++++++++++++++++++++++++++++++DLINKEDLIST CLASS+++++++++++++++++++++++++++++++++++++++++++++++++++
template<class Datatype>
class DLinkedList
{
  public:

    //++++++++++++++++++++++++++++++++++FUNCTIONS++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    //Constructor function
    //Sets the 3 attributes of the class to 0
    DLinkedList()
    {
      mHead = 0;
	  mTail = 0;
	  mCount = 0;
    }

    //Destructor- loops through all the nodes in the list and deletes each one
    ~DLinkedList()
    {
      DListNode<Datatype>* itr = mHead; //Set the first node- itr- to be the head node
	  DListNode<Datatype>* next; 

	  while(itr != 0)
	  {
        next = itr->mNext; //Set the node 'next' to be the next node of 'itr'
		delete itr; //Remove the node 'itr'
		itr = next; //'Move' the value of itr to be that of the next node
	  }
    }

    //'Append' function for adding player to end of the DLinkedList [also the start if there is no data in the list]
	//Takes in one parameter: 'p', of the Player class. This is the player object that will be stored in the last node of the DLinkedList
	void Append(Datatype d)
	{ 
      if(mHead == 0) //If mHead is empty [as it is by default], that means so nodes have been added to the List yet
	  {
        mHead = mTail = new DListNode<Datatype>; //When there's only one node in the List, then the head node is the tail node
		mHead->mData = d;
		mHead->mNext = 0; //There is no other node for the head node to point to (yet)- so set its 'mNext' pointer to 0
		mHead->mPrevious = 0; //Since there aren't going to be any nodes before the head node, the node's 'mPrevious' pointer will always be 0
	  }
	  else //List isn't empty- so add the new node to the end of the list
	  {
		mTail->InsertAfter(d); //Call InsertAfter function of the tail node to insert a new node with the data of 'p' in it after the tail node
		mTail = mTail->mNext; //The tail node is now the node after the original tail node
	  }

	  mCount++; //Number of nodes in the list has increased
	}

	//'Prepend' function for adding a player to the start of the DLinkedList.
	//Takes in one parameter: 'p', of the Player class. This is the player object that will be stored in the last node of the DLinkedList
	void Prepend(Datatype d)
	{
      if(mHead == 0) //mHead- is empty. Just like in 'Append', this simply involves setting both the head and tail to be this node
	  {
        mHead = mTail = new DListNode<Datatype>;
		mHead->mData = d;
		mHead->mNext = 0;
		mHead->mPrevious = 0;
	  }
	  else //Node(s) exist in the list
	  {
        //Not as simple as going to the end of the list and inserting a new node- 
		//A new node must be created, and its pointers set so that it comes before the head node
	    DListNode<Datatype>* newnode = new DListNode<Datatype>; //By default constructor, mHead and mTail of a created node point to 0

		//Use a temporary node to preserve the data and structure of the original mHead whilst a new one takes the place as mHead
		DListNode<Datatype>* x = new DListNode<Datatype>; 
	    
		newnode->mData = d; //Player data passed into newnode
	    newnode->mNext = mHead; //newnode placed before original head node- so that its 'next' pointer is the original mHead
		x = mHead; //Pass data and structure of the original mHead node into x
		mHead = newnode; //Appoint newnode as the new head node
		x->mPrevious = mHead; //Make x (the former head node)- which now comes after newnode- point to the new mHead
	  }

	  mCount++; //One more head has been added to the list
	}

	//'RemoveHead' function for removing the head node of the DLinkedList. No parameters or return type.
	void RemoveHead()
	{ 
	  if(mHead != 0)
	  {
	    DListNode<Datatype>* node = 0;
        node = mHead->mNext; //'node' gets the data of the next node after the head

		delete mHead; //Delete the head
		mHead = node; //'node' becomes the new head

		//If mHead points to '0'- meaning the node after the original mHead that is now the new mHead was empty- 
		//This means the space after original mHead was empty and thus signifies there is nothing left in the DLinkedList
		if(mHead == 0) 
		{
          mTail = 0; //So thus, the tail becomes empty too
		}

		mCount--; //There is one less node in the list
	  }
	} 

    //'RemoveTail' function for removing the last node from the DLinkedList. No parameters or return types
	void RemoveTail()
	{
	  if(mHead != 0) //A list with at least one node has a head node- so make sure list is not empty by checking it has a valid head node
	  {
        if(mHead == mTail) //When the head is the tail, that means there's only the one node in the List
		{
          delete mHead; //Thus remove the head too
		  mHead = mTail = 0;
		}
		else //More than one node exists in the list
		{
		  DListNode<Datatype>* node = mHead;
		  node = mTail->mPrevious;  //Set 'node' to be the node before the tail node

		  mTail = node; //'node' becomes the new mTail
		  delete node->mNext; //The node directly after 'node'- the original mTail- is deleted
		  node->mNext = 0; //Since 'node' is the new tail, set its mNext pointer to 0.
		}
		mCount--; //One node has been removed the list
	  }
	}

	//'GetIterator' function- simply returns a DListIterator using this DLinkedList as a parameter
	DListIterator<Datatype> GetIterator()
	{
      return DListIterator<Datatype>(this, mHead);
	}

    //'Insert' function- inserts a new list node for storing data after the node being pointed at by the iterator
	//Takes in 2 parameters:
	//1) 'iterator': the iterator that is pointing at the node that the new node is to be inserted after
	//2) 'p': the player data for the new node
    void InsertAfter(DListIterator<Datatype>& iterator, Datatype d)
    {
      if(iterator.mList != this) //'cancel' the function if the parameter passed into the function for 'iterator' is the wrong iterator
	  {
        return;
	  }

	  if(iterator.mNode != 0) //As an extra check, make sure the node being pointed to by the iterator isn't pointing to '0' before proceeding with the function code
	  {
        iterator.mNode->InsertAfter(d); //Insert the new node after the iterator's node

		//Make sure to update the tail node if the node being pointed at by the iterator the (former) tail node
		if(iterator.mNode == mTail)
		{
          mTail = iterator.mNode->mNext;
		}

		mCount++; //Increase mCount of the list

	  }
	  else //The iterator is NOT valid- so just append the data to the List
	  {
        Append(d);
	  }
	}

	//'InsertBefore' function for the DLinkedList- same as the 'InsertAfter' function, but adds a new node BEFORE the node the iterator is at
	//Takes in the same parameter as InsertAfter
	void InsertBefore(DListIterator<Datatype>& iterator, Datatype d)
    {
      if(iterator.mList != this)
	  {
        return;
	  }

	  if(iterator.mNode != 0)
	  {
        iterator.mNode->InsertBefore(d);

		//Make sure to update the head node if the node being pointed at by the iterator the (former) head node
		if(iterator.mNode == mHead)
		{
          mHead = iterator.mNode->mPrevious;
		}

		mCount++;
	  }
	  else //Iterator is NOT valid- so just prepend the data to the List
	  {
        Prepend(d);
	  }
	}

    //'Remove' function- removes the node being pointed at by an iterator from the DLinkedList
	//Takes in one parameter- the iterator to use
    void Remove(DListIterator<Datatype>& iterator)
    {
      if(iterator.mList != this) //'cancel' the function if the wrong iterator is being used
	  {
        return;
	  }
	  if(iterator.mNode == 0) //also 'cancel' the function if the node to be removed isn't valid
	  {
        return;
	  }

	  if(iterator.mNode == mHead) //The node to remove is the head node- so let the RemoveHead function deal with this
	  {
        iterator.Forth();
		RemoveHead();
	  }
	  else if(iterator.mNode == mTail) //If node to remove is tail node, just let RemoveTail deal with this
	  {
        RemoveTail();
	  }
	  else //Means that the node to remove is *somewhere in the middle*...
	  {
	    DListNode<Datatype>* node = mHead;
		node = iterator.mNode->mPrevious; //Make 'node' the previous node of the iterator

		iterator.Forth(); //Move the iterator forward so its node isn't deleted!
		delete node->mNext; //Remove the node after 'node' [the node to be removed]

        //Update both nodes adjacent to the node that was just removed

		node->mNext = iterator.mNode; //Where the iterator is pointing to NOW is the node before the removed node's new node
		iterator.mNode->mPrevious = node; //Since the node after 'node' has been removed, the previous pointer of the node at the iterator's current position is 'node'
		
        //Make sure to decrement mCount if the functions RemoveHead and RemoveTail weren't used; these decrement mCount anyway
		mCount--; 
	  }
    } 


	//--------------------------------------'InsertAtPosition' function---------------------------------------
	//This function looks for a specified position in the Doubly Linked List, and Inserts a new node at this position. 
	//[The position at which the node is to be placed will typically be a relative position to the iterator's node- either before or after an existing node
	//The function takes in 3 parameters- 
	//1)'p'- the Player data that the user wishes to insert into the List; 
	//2) 'position'- the position in the DLinkedList at which to insert the new node
	//3)'iter'- the iterator to be used in inserting the node at the required position
	//---------------------------------------------------------------------------------------------------------
    void InsertAtPosition(Datatype d, int position, DListIterator<Datatype>& iter)
	{
	  //Check if the value entered for the position equals 0 [or less, if the application is being used improperly]
      if(position <= 0) 
	  {
        //If so, the data should be added to the head of the List- so just use the Prepend function.
	    Prepend(d);
		iter.Start();
	  }
	  //Check if value entered is higher than the position of the last node in the List [mCount-1]
	  else if(position > (mCount-1))
	  {
        //If so, data is to be added to the tail of the List- so just use the Append function
	    cout << "ERROR: Requested position out of bounds; inserting after the last node instead" << endl;
		Append(d);
		iter.End();
	  }
	  else //Otherwise, the position is somewhere within the List- so a search algorithm must be set up to find it...
	  {
		if(iter.mList != this) //'cancel' the function if the wrong iterator is being used
		{
          return;
		}
	
        //The easiest option would be to either always start at the beginning of the list and iterate forwards to the appropriate position,
		//or always start at the end of the list and iterate backwards to the required position.
		//However, depending on the position entered by the user, one option might be more efficient that the other
		//So the position will be checked to see whether it is before the centre node in the list or after it
		//If before the centre node, it would be quicker to put the iterator at the start of the list and keep going forwards until it is found
		//If after the centre node, it would be quicker to put the iterator at the end and keep going backwards until it is found

		int midPos = (int)((0+mCount))/2; //Position of centre node is the midpoint between position of first node and after the last node
		bool forwards = true; //true if the iterator is to look forwards through the list; false if the iterator is to look backwards

		if(position <= midPos) //The position to insert a node at comes before or at the middle position in the list 
		{
		  //Quickest way to reach position is to start at the start and iterate forwards
          iter.Start(); //Put iterator at the start
		  while(iter.mPosition < position-1)
		  {
	        iter.Forth(); //Keep going FORWARD until the iterator is at the node before the target position
		  }
          //forwards = true; //Looked forwards through the list- so leave 'forwards' as true
		}

		else //Position to insert a node at comes after the middle position in the list
		{
		  //Quickest way to reach this position is to put iterator at the end and go backwards
		  iter.End(); //Put iterator at the end
		  while(iter.mPosition > position) 
		  {
			iter.Back(); //Keep going BACKWARDS until the iterator is at the node after the target position
		  }
		  forwards = false; //Looked backwards through the list- so set 'forwards' to false
		}

        //Call either 'InsertAfter' or 'InsertBefore', depending on whether the node before or after the target node was reached
        if(forwards)
		{
          //Going forwards from the start, the node before the target position was reached. 
		  //So InsertAfter will be used to place the node in position
		  InsertAfter(iter, d);
		}
		else
		{
          //Going backwards from the end, the node after the target position was reached. 
		  //So InsertBefore will be used to place node in position
		  InsertBefore(iter, d);
		}
	  }
	} 

	//-------------------------------------'RemoveAtPosition' function-------------------------------------
	//Similiar to 'InsertAtPosition', looks for a specified position in the Doubly Linked List, and deletes the node at that position
	//Takes in 2 parameters- 
	//1)'position'- the position of the node to remove from the list
	//2)'iter'- the iterator used to point to the node to remove
	//------------------------------------------------------------------------------------------------------
    void RemoveAtPosition(int position, DListIterator<Datatype>& iter)
	{
	  //First of all, check if the user is trying to remove a node from an empty list...
	  if(mCount == 0)
	  {
        cout << "Can't find the specified position. Maybe there are no nodes in the list..." << endl;
		return;
	  }

	  //Check if the position to remove is less or equal to the head node position, 0. 
	  //If so, just use the RemoveHead() function to remove the node
	  if(position <= 0)
	  {
        RemoveHead();
	  }

	  //Else check if position is greater than the position of the tail node, (mCount-1). If so, just use RemoveTail() function
	  else if(position >= mCount-1)
	  {
	    if(position > mCount-1)
		{
		  cout << "ERROR: Requested position out of bounds; removing last node instead" << endl;
		}
        RemoveTail();
	  }

	  else //Position to remove is somewhere within the List
	  {
        //Uses a similiar algorithm to locate the required position as 'InsertAtPosition' does

		int midPos = (int)((0+mCount))/2; //Position of centre node is the midpoint between position of first node and after the last node
		bool forwards = true; //true if the iterator is to look forwards through the list; false if the iterator is to look backwards

		if(position <= midPos) //The node to remove comes before or at the middle position in the list 
		{
		  //Quickest way to reach the position is to put iterator at the start and go forwards
          iter.Start(); //Put iterator at the start
		  while(iter.mPosition < position)
		  {
	        iter.Forth(); //Keep going FORWARD until the iterator reaches the node to remove
		  }
		}
		else //Node to remove comes after the middle position in the list
		{
		  //Quickest way to reach this position is to put iterator at the end and go backwards
		  iter.End(); //Put iterator at the end
		  while(iter.mPosition > position)
		  {
			iter.Back(); //Keep going BACKWARDS until the iterator reaches the node to remove
		  }
		}

        Remove(iter); //Remove the node
	  } 
	} 

	//++++++++++++++++++++++++++++++++ATTRIBUTES++++++++++++++++++++++++++++++++

	DListNode<Datatype>* mHead; //The 'head node'- first node of the list
	DListNode<Datatype>* mTail; //The 'tail node'- last node of the list
	int mCount; //Number of nodes in the list
};

#endif