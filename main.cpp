//Entry point for the application

#include "LeesEngine\Player.h"
#include "LeesEngine\Map.h"
#include "LeesEngine\GameServer.h"
#include "LeesEngine\TimeManager.h"
#include "LeesEngine\GameData.h"

#include <iostream>

using namespace std;
using namespace LeesEngine;
using namespace ThreadLib;

// **Thread for game updater **/
void gameEventHandler(void* data)
{
	TimeManager* timer = (TimeManager*)data;

	while(timer->isRunning() )
	{
		timer->updateEnemies(); //...Will probably have to expand to update everything...
	}
}

int main()
{
   cout << "And this is the step that begins the journey of thousands of lines..." << endl;

   /*
   //vector<Item> bobsInventory;
   //vector<string> bobsAllies;
   //ActorAttributes bobsAttributes = {10, 20, 30, 40, 50, 60, 70};
   //Player bob("Bob", 100, bobsAttributes, 2, 0, 0, "human", 9001, 100, bobsInventory, bobsAllies, "regular");

   //Array<string> players(25, 0);
   //Array<Enemy> enemies(25, 0);
   //Array<Item> items(25, 0);
   Exits exits = {1, 2, -1, -1};

   //Room room(0, "regular", 1, players, enemies, items, exits, "a very boring room. Ta-dah");
   Room room(0, "regular", 1, exits, "a very boring room");

   //Array<Room> rooms(34, 5);
   //rooms.Replace(room, 1);
   //rooms.Push(room);
   //rooms[1] = room;
   Map map( true );
   //Map map3( true );
   //Map map4( true );
   //Map map5( true );

   //cout << map.getRoom(0).getDescription() << endl;

   //cout << rooms[0].getDescription() << endl;

   //cout << rooms[1].getDescription() << endl;

   //cout << map. .getItem(12).getDescription() << endl;
   map.setRoom(room, 30);

   bob.attack(bob);

   cout << bob.getCharisma() << endl;*/

   //To do : 
   // 1) Load game data
   // 2)set up GameServer here,
   //as well as possibly a thread for the TimeManager

   /** Load game data into GameData **/

   GameData::setupData();

   /** Set up server **/
   short ports[] = {9001, 9002, 9003, 9004, 9005};
   GameServer server(ports, 5);
   bool b_gameInProg = true;

   TimeManager tm(0);
   ThreadID timeRunner = ThreadLib::Create( gameEventHandler, (void*)&tm);

   while(b_gameInProg)
   {
      //If terminating event occurs --> b_gameInProg = false
	  // ... though a server exit would just kill this anyway, wouldn't it?
	  server.runServer();
   }


   return 0;
}