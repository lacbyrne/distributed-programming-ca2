/**
   For classes that send string messages around
   This setup works better than, say, 'if (method_output == " specific string" ) ',
   as there may be a desire to change the exact messages at a later stage
**/

#include <string>

using std::string;

namespace LeesEngine
{
	class Messages
	{
	   public:
	      const static string Login;
	      const static string WrongPassword;
	      const static string AlreadyLoggedin;
	      const static string NewPlayer;
	      const static string IllegalName;
	};
}